FROM golang AS build
WORKDIR /app
ADD go.* ./
RUN go mod download
COPY . ./
RUN ls -la /app
RUN CGO_ENABLED=0 GOOS=linux go build -o /app/bin/ci-cd /app/cmd/serve

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /app/
COPY --from=build /app/bin ./
CMD ["./ci-cd"]