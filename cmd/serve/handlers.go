package main

import (
	"encoding/json"
	"net/http"
	"net/http/httputil"
	"time"

	"ci-cd/internal/tweet"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/olivere/elastic/v7"
)

func RegisterHealthCheck(r *mux.Router, client *elastic.Client) {
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		_, err := client.CatIndices().Do(request.Context())
		if err != nil {
			writer.WriteHeader(http.StatusServiceUnavailable)
			return
		}

		writer.WriteHeader(http.StatusOK)
		writer.Write([]byte("ok: version 6"))
	})
}

func RegisterDebug(r *mux.Router) {
	r.HandleFunc("/debug", func(writer http.ResponseWriter, request *http.Request) {
		dumped, err := httputil.DumpRequest(request, true)
		if err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			writer.Write([]byte("failed to dump request"))
			return
		}

		writer.WriteHeader(http.StatusOK)
		writer.Write(dumped)
	})
}

type TweetsResponse struct {
	Tweets []tweet.Tweet `json:"tweets" bson:"tweets"`
}

func RegisterGetHandler(r *mux.Router, searcher tweet.Searcher) {
	r.HandleFunc("/tweets/{search}", func(writer http.ResponseWriter, request *http.Request) {
		v := mux.Vars(request)

		tweets, err := searcher.Search(request.Context(), v["search"])
		if err != nil {
			writer.WriteHeader(400)
			writer.Write([]byte("error: " + err.Error()))
			return
		}

		data, err := json.Marshal(TweetsResponse{Tweets: tweets})
		if err != nil {
			writer.WriteHeader(400)
			writer.Write([]byte("error: " + err.Error()))
			return
		}

		writer.WriteHeader(200)
		writer.Write(data)
	})
}

type TweetResponse struct {
	Tweet tweet.Tweet `json:"tweet" bson:"tweet"`
}

func RegisterCreateHandler(r *mux.Router, indexer tweet.Indexer) {
	r.HandleFunc("/tweets", func(writer http.ResponseWriter, request *http.Request) {
		var tw tweet.Tweet

		if err := json.NewDecoder(request.Body).Decode(&tw); err != nil {
			writer.WriteHeader(400)
			writer.Write([]byte("error: " + err.Error()))
			return
		}

		tw.ID = uuid.New().String()
		tw.CreatedAt = time.Now()

		if err := indexer.Index(request.Context(), tw); err != nil {
			writer.WriteHeader(400)
			writer.Write([]byte("error: " + err.Error()))
			return
		}

		data, err := json.Marshal(TweetResponse{Tweet: tw})
		if err != nil {
			writer.WriteHeader(400)
			writer.Write([]byte("error: " + err.Error()))
			return
		}

		writer.WriteHeader(200)
		writer.Write(data)
	})
}
