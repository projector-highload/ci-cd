package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"ci-cd/internal/tweet"

	"github.com/gorilla/mux"
	"github.com/olivere/elastic/v7"
)

func main() {
	dsn := os.Getenv("ELASTIC_DSN")
	if dsn == "" {
		dsn = "http://127.0.0.1:9200"
	}

	logger := log.New(os.Stdout, "cd-cd: ", log.LstdFlags)

	client, err := elastic.NewClient(
		elastic.SetURL(dsn),
		elastic.SetErrorLog(logger),
		elastic.SetTraceLog(logger),
		elastic.SetInfoLog(logger),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
	)
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	setuper := tweet.NewSetuper(client)
	if err := setuper.Setup(ctx); err != nil {
		panic(err)
	}

	searcher := tweet.NewSearcher(client)
	indexer := tweet.NewIndexer(client)

	r := mux.NewRouter()

	RegisterHealthCheck(r, client)
	RegisterDebug(r)
	RegisterCreateHandler(r, indexer)
	RegisterGetHandler(r, searcher)

	srv := &http.Server{
		Addr:         "0.0.0.0:80",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	<-c

	wait := time.Second * 3
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	srv.Shutdown(ctx)

	log.Println("shutting down")
	os.Exit(0)
}
