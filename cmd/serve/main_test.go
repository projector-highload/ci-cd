package main

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"ci-cd/internal/tweet"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type searcher struct {
	mock.Mock
}

func (f *searcher) Search(ctx context.Context, searchStr string) ([]tweet.Tweet, error) {
	args := f.Called(searchStr)
	return args.Get(0).([]tweet.Tweet), args.Error(1)
}

func TestOne(t *testing.T) {
	s := &searcher{}

	ctx := context.Background()

	tweets := []tweet.Tweet{
		{
			ID:      uuid.New().String(),
			Message: "test message",
		},
	}

	s.On("Search", "test").Return(tweets, nil)

	r := mux.NewRouter()

	RegisterGetHandler(r, s)

	res := executeRequest(r, (&http.Request{Method: "GET", URL: &url.URL{Path: "/tweets/test"}}).WithContext(ctx))

	var resp TweetsResponse
	assert.NoError(t, json.NewDecoder(res.Body).Decode(&resp))
	assert.Len(t, resp.Tweets, 1)

	assert.NoError(t, nil)
}

func executeRequest(r *mux.Router, req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	return rr
}
