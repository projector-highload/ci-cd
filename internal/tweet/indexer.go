package tweet

import (
	"context"

	"github.com/olivere/elastic/v7"
)

type Indexer interface {
	Index(ctx context.Context, tweet Tweet) error
}

func NewIndexer(client *elastic.Client) Indexer {
	return &indexer{client: client}
}

type indexer struct {
	client *elastic.Client
}

func (i *indexer) Index(ctx context.Context, tweet Tweet) error {
	_, err := i.client.Index().
		Index(indexName).
		Id(tweet.ID).
		BodyJson(tweet).
		Do(ctx)

	return err
}
