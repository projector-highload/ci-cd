package tweet

import (
	"context"
	"reflect"
	"time"

	"github.com/olivere/elastic/v7"
)

type GeoPoint struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lon"`
}

type Tweet struct {
	ID           string    `json:"id"`
	User         string    `json:"user"`
	Message      string    `json:"message"`
	Image        string    `json:"image"`
	CreatedAt    time.Time `json:"created_at"`
	Tags         []string  `json:"tags"`
	Location     GeoPoint  `json:"location"`
	SuggestField string    `json:"suggest_field"`
}

type Searcher interface {
	Search(ctx context.Context, searchStr string) ([]Tweet, error)
}

func NewSearcher(client *elastic.Client) Searcher {
	return &searcher{client: client}
}

type searcher struct {
	client *elastic.Client
}

func (s *searcher) Search(ctx context.Context, searchStr string) ([]Tweet, error) {
	termQuery := elastic.NewMatchQuery("message", searchStr).Fuzziness("AUTO")

	searchResult, err := s.client.Search().
		Index(indexName).
		Query(termQuery).
		// Suggester(s.suggester(searchStr)).
		From(0).
		Size(10).
		Pretty(true).
		Do(ctx)
	if err != nil {
		return nil, err
	}

	tweets := make([]Tweet, 0)
	var t Tweet
	for _, r := range searchResult.Each(reflect.TypeOf(t)) {
		tweets = append(tweets, r.(Tweet))
	}

	return tweets, nil
}

func (s *searcher) suggester(searchStr string) elastic.Suggester {
	return elastic.
		NewCompletionSuggester("autocomplete").
		Field("message.complete").
		Prefix(searchStr).
		Fuzziness("AUTO")
}
