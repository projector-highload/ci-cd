package tweet

import (
	"context"

	"github.com/olivere/elastic/v7"
)

const indexName = "twitter"

const mapping = `
{
  "settings": {
    "number_of_shards": 10,
    "analysis": {
      "analyzer": {
        "autocomplete": {
          "tokenizer": "autocomplete",
          "filter": [
            "lowercase"
          ]
        },
        "autocomplete_search": {
          "tokenizer": "lowercase"
        }
      },
      "tokenizer": {
        "autocomplete": {
          "type": "edge_ngram",
          "min_gram": 2,
          "max_gram": 10,
          "token_chars": [
            "letter"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "user": {
        "type": "keyword"
      },
      "message": {
        "type": "text",
        "store": true,
        "analyzer": "autocomplete",
        "search_analyzer": "autocomplete_search",
		"fields": {
			"complete": {
				"type": "completion"
			}
		}
      },
      "image": {
        "type": "keyword",
        "store": true
      },
      "created": {
        "type": "date",
        "store": true
      },
      "tags": {
        "type": "keyword",
        "store": true
      },
      "location": {
        "type": "geo_point",
        "store": true
      },
      "suggest_field": {
        "type": "search_as_you_type",
        "store": true
      }
    }
  }
}`

type Setuper interface {
	Setup(ctx context.Context) error
}

func NewSetuper(client *elastic.Client) Setuper {
	return &settuper{client: client}
}

type settuper struct {
	client *elastic.Client
}

func (s *settuper) Setup(ctx context.Context) error {
	exists, err := s.client.IndexExists(indexName).Do(ctx)
	if err != nil {
		return err
	}

	if exists {
		return nil
	}

	_, err = s.client.
		CreateIndex(indexName).
		BodyString(mapping).
		Do(ctx)

	return err
}
