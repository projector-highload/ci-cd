package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"ci-cd/internal/tweet"

	"github.com/stretchr/testify/assert"
)

type tweetResponse struct {
	Tweet tweet.Tweet `json:"tweet" bson:"tweet"`
}

type tweetsResponse struct {
	Tweets []tweet.Tweet `json:"tweets" bson:"tweets"`
}

func TestCreateAndGet(t *testing.T) {
	appURL := os.Getenv("APP_URL")
	if appURL == "" {
		appURL = "http://127.0.0.1:8080"
	}

	tw := makeTweet()

	data, _ := json.Marshal(tw)

	resp, err := http.Post(fmt.Sprintf("%s/tweets", appURL), "application/json", bytes.NewReader(data))
	if !assert.NoError(t, err) {
		t.FailNow()
	}
	var createResp tweetResponse
	assert.NoError(t, json.NewDecoder(resp.Body).Decode(&createResp))
	assert.NotEmpty(t, createResp.Tweet.ID)

	time.Sleep(time.Second)

	resp, err = http.Get(fmt.Sprintf("%s/tweets/super", appURL))
	if !assert.NoError(t, err) {
		t.FailNow()
	}
	var searchResp tweetsResponse
	if !assert.NoError(t, json.NewDecoder(resp.Body).Decode(&searchResp)) {
		t.FailNow()
	}
	assert.NotEmpty(t, searchResp.Tweets)
	assert.Len(t, searchResp.Tweets, 1)
}

func makeTweet() tweet.Tweet {
	return tweet.Tweet{
		Message: "super tweet",
		User:    "John Wick",
		Tags:    []string{"test"},
	}
}
